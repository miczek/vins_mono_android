import os

import gmplot
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import utm
from scipy.spatial.transform import Rotation
import yaml

# Matplotlib/Seaborn config
sns.set_style('whitegrid')
# plt.rcParams["figure.figsize"] = (15, 13)
point_size = 0.1

# Hardcoded relative directories and filenames
data_directory = 'test_drive_manu_08_12/pixel_arcore/6'
data_filename = 'odom_result.csv'
gps_filename = 'gps.csv'
resource_usage_filename = 'resource_usage.csv'
plot_config_filename = '../plot_config.yaml'

# Read the data
odometry = pd.read_csv(os.path.join(data_directory, data_filename))
gps_data = pd.read_csv(os.path.join(data_directory, gps_filename))
resource_usage = pd.read_csv(os.path.join(data_directory, resource_usage_filename))

with open(os.path.join(data_directory, plot_config_filename), 'r') as stream:
    try:
        plot_config = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)
        exit(0)

# Bools that specify which plots should be created and stored to files
save_all = plot_config['save_all']
plot_xy_from_above = plot_config['plot']['xy_from_above']
plot_rpy = plot_config['plot']['rpy']
plot_on_map = plot_config['plot']['on_map']
compare_some_stuff = plot_config['plot']['some_stuff']
plot_resource_usage = plot_config['plot']['resource_usage']

if plot_config['coordinate_system'] == 'android':
    # change to "normal" coordinate frames - x forward, y left, z up
    odometry['prev_y'] = odometry['y']
    odometry['y'] = -odometry['z']
    odometry['z'] = odometry['prev_y']

odometry['timestamp'] = (odometry['timestamp'] / 1000) % 10000
gps_data['timestamp'] = (gps_data['timestamp'] / 1000) % 10000
resource_usage['timestamp'] = (resource_usage['timestamp'] / 1000) % 10000
# odometry = odometry[odometry.index % 50 == 0]

max_allowed_interval_without_data = plot_config['max_allowed_interval']

if compare_some_stuff or save_all:
    fig, ax = plt.subplots(2)
    ax[0].set_title('GPS speed vs raw x,y,z odom estimates')
    ax[0].set_ylabel('v[m/s]')
    ax[0].plot(gps_data.timestamp - odometry.iloc[0]['timestamp'],
               gps_data.speed, label='GPS speed')  # , c=np.random.rand(3,))
    ax[0].legend()
    ax[1].set_ylabel('s[m]')
    ax[1].set_xlabel('t[s]')
    ax[1].scatter(odometry.timestamp - odometry.iloc[0]['timestamp'], odometry.x, s=0.1, label='Estimated x')
    ax[1].scatter(odometry.timestamp - odometry.iloc[0]['timestamp'], odometry.y, s=0.1, label='Estimated y')
    ax[1].scatter(odometry.timestamp - odometry.iloc[0]['timestamp'], odometry.z, s=0.1, label='Estimated z')
    ids = (odometry["timestamp"] > (odometry["timestamp"].shift() + max_allowed_interval_without_data)).cumsum()
    grouped = odometry.groupby(ids)
    for index, group in grouped:
        plot_x_coord = group.timestamp.iloc[-1] - odometry.iloc[0].timestamp
        plot_y_coord = 0.0
        gps_interrupt_index = gps_data.timestamp.searchsorted(group.timestamp.iloc[-1])
        if gps_interrupt_index < len(gps_data):
            gps_speed_at_interrupt = gps_data.iloc[gps_interrupt_index].speed
            print(gps_speed_at_interrupt)
            ax[1].scatter(plot_x_coord, plot_y_coord, marker='x', color='red')

            ax[1].annotate(f'{gps_speed_at_interrupt:.1f}',
                           (group.timestamp.iloc[-1] - odometry.iloc[0].timestamp, 0.0))
    ax[1].legend()
    fig.savefig(os.path.join(data_directory, 'speed_vs_xyz.jpg'), format='jpg')
    plt.clf()

if plot_on_map or save_all:
    map_config = plot_config['on_map']
    COLOR_RED = '#FF0000'
    COLOR_BLUE = '#0000FF'
    mean_lat = gps_data['latitude'].mean()
    mean_lng = gps_data['longitude'].mean()
    # Google Maps plot config, view centered at the mean lat/long coords
    gmap = gmplot.GoogleMapPlotter(lat=mean_lat, lng=mean_lng, zoom=map_config['zoom'])

    gmap.scatter(lats=gps_data['latitude'], lngs=gps_data['longitude'], color=COLOR_RED, size=1, marker=False)

    gps_for_v = gps_data.copy()
    if map_config['add_gps_timestamps']:
        gps_for_v.apply(lambda row: gmap.text(lat=row['latitude'], lng=row['longitude'],
                                              text=f't={row["timestamp"]-gps_data["timestamp"].iloc[0]:.1f}'), axis=1)
    if map_config['add_gps_speed']:
        gps_for_v.apply(lambda row: gmap.text(lat=row['latitude'], lng=row['longitude'],
                                              text=f'v={row["speed"]:.1f}'), axis=1)
    gmap.scatter(lats=gps_data['latitude'], lngs=gps_data['longitude'], color=COLOR_RED, size=1, marker=False)

    # Split the odom readings into groups if more time than max_allowed_interval_without_data_ms has passed between two data readings
    ids = (odometry["timestamp"] > (odometry["timestamp"].shift() + max_allowed_interval_without_data)).cumsum()
    grouped = odometry.groupby(ids)

    # initialize the corrections with zeroes, overwrite only entries with specified indices
    x_y_array = [(0.0, 0.0)] * len(grouped)
    angle_array = [0.0] * len(grouped)
    for index, alpha, x, y in map_config['subpath_correction']:
        x_y_array[index] = (x, y)
        angle_array[index] = alpha
    """
    correction_array[0] = 135.0
    correction_array[1] = 140
    offset_array[1] = (0.0, 0.0)
    correction_array[2] = 140
    offset_array[2] = (32.0, 7.0)
    correction_array[3] = 140
    correction_array[4] = 140
    offset_array[4] = (-20.0, -25.0)
    correction_array[5] = 140
    correction_array[6] = 140
    offset_array[6] = (8.0, -40.0)
    correction_array[7] = 140
    correction_array[8] = 130
    """
    for index, group in grouped:
        print(index)
        print_color = COLOR_BLUE
        if map_config['use_random_colors']:
            b = np.random.randint(low=0, high=0xff+1)
            g = np.random.randint(low=0, high=0xff+1) * 0x100
            r = np.random.randint(low=0, high=0xff+1) * 0x10000
            print(f'{hex(r)=}{hex(g)=}{hex(b)=}')
            print(f'{hex(r+g+b)=}')
            print_color = '#' + hex(r+g+b)[2:]
            print(print_color)

        # Reset group's origin to (0,0,0)
        group['x'] -= group['x'].iloc[0]
        group['y'] -= group['y'].iloc[0]
        group['z'] -= group['z'].iloc[0]

        # Take the first GPS timestamp that came after the first odom timestamp as the origin
        init_gps_index = gps_data[gps_data['timestamp'].gt(group['timestamp'].iloc[0])].index[0]
        init_lat = gps_data['latitude'].iloc[init_gps_index]
        init_lng = gps_data['longitude'].iloc[init_gps_index]

        # Transformation to a local tangential plane (UTM zone)
        start_x, start_y, zone_num, zone_letter = utm.from_latlon(init_lat, init_lng)

        odometry_for_gps = group[['timestamp', 'x', 'y', 'z']].reset_index().copy()
        bearing_correction = angle_array[index]

        rot: Rotation = Rotation.from_euler('XYZ', [0.0, 0.0, bearing_correction], degrees=True)

        # Apply the initial rotation
        rotated = rot.apply(odometry_for_gps[['x', 'y', 'z']])
        odometry_for_gps[['x', 'y', 'z']] = pd.DataFrame(rotated, columns=['x', 'y', 'z'])

        x_offset = x_y_array[index][0]  # Adjust to compensate for wrong GPS readings; positive means go left
        y_offset = x_y_array[index][1]  # Adjust compensate for wrong GPS readings, positive means go up

        # Add global offset to local cartesian coordinates on the tangent plane
        odometry_for_gps['x'] += start_x + x_offset
        odometry_for_gps['y'] += start_y + y_offset

        # Transform back to geodesian coordinates
        odometry_for_gps['lat_lon'] = odometry_for_gps.apply(
            lambda row: utm.to_latlon(row['x'], row['y'], zone_num, zone_letter), axis=1)

        # Extract the values of lat/long for plotting
        odometry_for_gps['latitude'] = odometry_for_gps['lat_lon'].apply(lambda row: row[0])
        odometry_for_gps['longitude'] = odometry_for_gps['lat_lon'].apply(lambda row: row[1])

        if map_config['add_subpath_ids']:
            gmap.text(odometry_for_gps['latitude'].mean(),
                      odometry_for_gps['longitude'].mean(), f'part {index}', color=print_color)

        # Reduce number of data points to decrease the calculation time
        odometry_for_gps = odometry_for_gps[odometry_for_gps.index % 50 == 0]
        if map_config['add_odom_timestamps']:
            odometry_for_gps.apply(lambda row: gmap.text(lat=row['latitude'], lng=row['longitude'],
                                                         text=f't={row["timestamp"]-odometry["timestamp"].iloc[0]:.1f}'), axis=1)

        gmap.scatter(lats=odometry_for_gps['latitude'],
                     lngs=odometry_for_gps['longitude'], color=print_color, size=1, marker=False)
    gmap.draw(file=os.path.join(data_directory, f'arcore_vs_gps_vel2.html'))

    if plot_xy_from_above or save_all:
        fig, ax = plt.subplots()
        #ax.set_xlim([-120, 120])
        #ax.set_ylim([-120, 120])
        ax.set_aspect('equal')
        bearing_correction = 0  # np.deg2rad(gps_data['bearing'].iloc[15:30].median())
        odometry_for_gps = odometry[['x', 'y', 'z']].copy()
        rot: Rotation = Rotation.from_euler('XYZ', [0.0, 0.0, bearing_correction], degrees=True)

        # Align the initial rotations
        rotated = rot.apply(odometry_for_gps[['x', 'y', 'z']])
        odometry_for_gps[['x', 'y', 'z']] = pd.DataFrame(rotated, columns=['x', 'y', 'z'])
        ax.scatter(odometry_for_gps['x'],
                   odometry_for_gps['y'], label='ARCore estimate', s=point_size)
        ax.legend()
        ax.set_title('Estimated xy coordinates')
        ax.set_xlabel('x [m]')
        ax.set_ylabel('y [m]')
        fig.savefig(os.path.join(data_directory, f'xy_from_above_rot_{bearing_correction}.jpg'), format='jpg', dpi=300)
#        plt.show()
        plt.clf()

if plot_rpy or save_all:
    fig, ax = plt.subplots()

    ax.scatter(odometry['timestamp'],
               odometry['roll_deg'], label='Estimated roll', s=point_size)
    ax.scatter(odometry['timestamp'],
               odometry['pitch_deg'], label='Estimated pitch', s=point_size)
    ax.scatter(odometry['timestamp'],
               odometry['yaw_deg'], label='Estimated yaw', s=point_size)
    ax.legend()
    ax.set_title('Estimated orientation')
    ax.set_xlabel('time [s]')
    ax.set_ylabel('degrees')
    fig.savefig(os.path.join(data_directory, 'rpy_deg.jpg'), format='jpg')
    # plt.show()
    plt.clf()

if plot_resource_usage or save_all:
    fig, ax = plt.subplots(2)
    ax[0].plot(resource_usage['timestamp'], resource_usage['cpu_percent'], label='Usage of 1 CPU')
    ax[1].plot(resource_usage['timestamp'], resource_usage['memory_percent'], label='Memory')
    ax[0].set_title('Usage of resources')
    ax[0].set_ylabel('% of one CPU (max 800)')
    ax[1].set_ylabel('% of RAM')
    ax[1].set_xlabel('time [s]')
    fig.savefig(os.path.join(data_directory, 'resource_usage.jpg'), format='jpg')
    plt.clf()
