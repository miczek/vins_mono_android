package com.thkoeln.jmoeller.vins_mobile_androidport;

import android.media.Image;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.concurrent.locks.ReentrantLock;

public class SwapImageBuffer {
    private final ReentrantLock lock = new ReentrantLock();
    private Image[] mBuffer;
    private boolean[] mReadBuffer;
    private int mCurrentIndex = 0;
    private boolean mIsEmpty = true;
    private int mBufferLength = 2;
    private final String TAG = "SwapImageBuffer";

    public SwapImageBuffer() {
        mBuffer = new Image [mBufferLength];
        mReadBuffer = new boolean [mBufferLength];
        for (int i = 0; i < mBufferLength; i++) {
            mBuffer[i] = null;
            mReadBuffer[i] = false;
        }
    }
    public void acquireLock() {
        lock.lock();
    }
    public void freeLock() {
        lock.unlock();
    }

    public void setBufferValue(Image image) {
        Log.i(TAG, "Enter setBufferValue");
        int nextIndex = (mCurrentIndex + 1) % mBufferLength;
        // If buffer not empty and element had not been read, close it and free it before storing the next image.
        // If it has been read, the user should take care of freeing it instead
        Log.i(TAG, "Check in setBufferValue");
        if(mBuffer[nextIndex] != null && !mReadBuffer[nextIndex]) {
            Log.i(TAG, "Close image");
            mBuffer[nextIndex].close();
        }
        mBuffer[nextIndex] = image;
        mReadBuffer[nextIndex] = false;
        mCurrentIndex = nextIndex;
        if (mIsEmpty) {
            mIsEmpty = false;
        }
    }

    public Image getBufferValue() {
        if (mIsEmpty || mReadBuffer[mCurrentIndex]) {
            return null;
        }
        mReadBuffer[mCurrentIndex] = true;
        return mBuffer[mCurrentIndex];
    }
}
