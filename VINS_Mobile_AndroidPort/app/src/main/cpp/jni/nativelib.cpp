#include <jni.h>
#include <string>
#include <android/log.h>
#include <time.h>
#include <ViewController.hpp>

#define LOG_TAG "nativelip.cpp"

// debug logging
#define LOGI(...)  __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#define LOGE(...)  __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

#include <android/native_window.h>
#include <android/native_window_jni.h>
cv::Mat convertToCvRGB(AndroidImage const & androidImg);
// global variable to viewController 
// because its much less of a hassle 
// than to pass pointers to Java and back
std::unique_ptr<ViewController> viewControllerGlobal;

extern "C"
JNIEXPORT void JNICALL
Java_com_thkoeln_jmoeller_vins_1mobile_1androidport_VinsJNI_init(JNIEnv *env, jobject instance) {
    
    viewControllerGlobal = std::unique_ptr<ViewController>(new ViewController);
    LOGI("Successfully created Viewcontroller Object");
    
    viewControllerGlobal->testMethod();
    
    // startup method of ViewController
    viewControllerGlobal->viewDidLoad();
}

extern "C"
JNIEXPORT void JNICALL
Java_com_thkoeln_jmoeller_vins_1mobile_1androidport_VinsJNI_onImageAvailable(JNIEnv *env, jclass type,
                                                                           jint width, jint height,
                                                                           jint rowStrideY,
                                                                           jint pixelStrideY,
                                                                           jobject bufferY,
                                                                           jint rowStrideUV,
                                                                           jint pixelStrideUV,
                                                                           jobject bufferU,
                                                                           jobject bufferV,
                                                                           jobject surface,
                                                                           jlong timeStamp,
                                                                           jboolean isScreenRotated,
                                                                           jfloat virtualCamDistance) {

//    LOGI("Received image with width: %d height: %d", width, height);
    uint8_t *y_ptr = reinterpret_cast<uint8_t *>(env->GetDirectBufferAddress(bufferY));
    uint8_t *u_ptr = reinterpret_cast<uint8_t *>(env->GetDirectBufferAddress(bufferU));
    uint8_t *v_ptr = reinterpret_cast<uint8_t *>(env->GetDirectBufferAddress(bufferV));
    if (y_ptr == nullptr || u_ptr == nullptr || v_ptr == nullptr) {
        LOGE("image buffer null pointer error");
        return;
    }
    AndroidPlane plane_y { y_ptr, pixelStrideY, rowStrideY };
    AndroidPlane plane_u { u_ptr, pixelStrideUV, rowStrideUV };
    AndroidPlane plane_v { v_ptr, pixelStrideUV, rowStrideUV };

    AndroidImage android_image;
    android_image.channels[0] = plane_y;
    android_image.channels[1] = plane_u;
    android_image.channels[2] = plane_v;
    android_image.height = height;
    android_image.width = width;

    double timeStampSec = ViewController::timeStampToSec(timeStamp);
    android_image.timestamp = timeStampSec;
    // IMU Meassurements are momentary meassurements. 
    // Camera over an interval. so the mid of the interval is chosen as the timestamp
    // Half the maximum exposure time - half senor time delta
    const double timeStampOffset = 1.0 / 30.0 / 2.0 - 1.0 / 100.0 / 2.0;
    timeStampSec += timeStampOffset;
    
    LOGI("Current ImageTimestamp - IMUTimeStamp: %lf with offset: %lf",
         timeStampSec - viewControllerGlobal->getLateast_imu_time(),
         timeStampOffset);

    ANativeWindow *win = ANativeWindow_fromSurface(env, surface);
    
    ANativeWindow_acquire(win);
    /** buffer which provides a direct access to the display (right?) **/
    ANativeWindow_Buffer buf;

    int rotatedWidth = height; // 480
    int rotatedHeight = width; // 640

    ANativeWindow_setBuffersGeometry(win, width, height, 0);

    if (int32_t err = ANativeWindow_lock(win, &buf, NULL)) {
        LOGE("ANativeWindow_lock failed with error code %d\n", err);
        ANativeWindow_release(win);
        return;
    }

    uint8_t *dstPtr = reinterpret_cast<uint8_t *>(buf.bits);
    Mat dstRgba(height, buf.stride, CV_8UC4, dstPtr); // TextureView buffer, use stride as width
    Mat srcRgba(height, width, CV_8UC4);
    Mat rotatedRgba(rotatedHeight, rotatedWidth, CV_8UC4);
    //cv::Mat gray = cv::Mat(height, width, CV_8UC1, y_ptr);
    //cv::cvtColor(gray, srcRgba, cv::COLOR_GRAY2RGBA);

    TS(actual_onImageAvailable);

    // convert YUV to RGB which actually is BGR in OpenCV
    srcRgba = convertToCvRGB(android_image);
    cv::cvtColor(srcRgba, srcRgba, cv::COLOR_BGR2RGBA);
    // Rotate 90 degree
    cv::rotate(srcRgba, rotatedRgba, cv::ROTATE_90_CLOCKWISE);

    assert(rotatedRgba.size().width == 480);
    assert(rotatedRgba.size().height == 640);
    viewControllerGlobal->virtualCamDistance = (float)virtualCamDistance;
    viewControllerGlobal->processImage(rotatedRgba, timeStampSec, isScreenRotated);
    LOGI("rotate");
    cv::rotate(rotatedRgba, srcRgba, cv::ROTATE_90_COUNTERCLOCKWISE);
    // copy to TextureView surface
    uchar *dbuf = dstRgba.data;
    uchar *sbuf = srcRgba.data;
    int i;
    LOGI("memcpy");
    for (i = 0; i < srcRgba.rows; i++) {
        dbuf = dstRgba.data + i * buf.stride * 4;
        memcpy(dbuf, sbuf, srcRgba.cols * 4); //TODO: threw a SIGSEGV SEGV_ACCERR once
        sbuf += srcRgba.cols * 4;
    }
    //memcpy(dbuf, sbuf, srcRgba.total() * srcRgba.elemSize());
    TE(actual_onImageAvailable);
    
    ANativeWindow_unlockAndPost(win);
    ANativeWindow_release(win);
    LOGI("Released");

}

extern "C"
JNIEXPORT void JNICALL
Java_com_thkoeln_jmoeller_vins_1mobile_1androidport_VinsJNI_onPause(JNIEnv *env, jclass type) {
    LOGI("Pause triggered, stopping SensorEvents");
    viewControllerGlobal->imuStopUpdate();
}

// Constants for ImageView visibility coming from Java
const int VISIBLE = 0x00000000;
const int INVISIBLE = 0x00000004;
extern "C"
JNIEXPORT jstring JNICALL
Java_com_thkoeln_jmoeller_vins_1mobile_1androidport_VinsJNI_getLogLine(JNIEnv *env, jclass type) {
    return env->NewStringUTF(viewControllerGlobal->csv_line.c_str());
}
extern "C"
JNIEXPORT void JNICALL
Java_com_thkoeln_jmoeller_vins_1mobile_1androidport_VinsJNI_updateViewInfo(JNIEnv *env, jclass type,
                                                                         jobject tvX, jobject tvY,
                                                                         jobject tvZ,
                                                                         jobject tvTotal,
                                                                         jobject tvLoop,
                                                                         jobject tvFeature,
                                                                         jobject tvBuf,
                                                                         jobject ivInit) {

    // Get the method handles
    jclass tvClass = env->FindClass("android/widget/TextView");
    jmethodID setTextID = env->GetMethodID(tvClass, "setText", "(Ljava/lang/CharSequence;)V");

    jclass ivClass = env->FindClass("android/widget/ImageView");
    jmethodID setVisibilityID = env->GetMethodID(ivClass, "setVisibility", "(I)V");
    
    viewControllerGlobal->viewUpdateMutex.lock();
    if(viewControllerGlobal->tvXText.empty() == false) {
        env->CallVoidMethod(tvX, setTextID, env->NewStringUTF(viewControllerGlobal->tvXText.c_str()));
        env->CallVoidMethod(tvY, setTextID, env->NewStringUTF(viewControllerGlobal->tvYText.c_str()));
        env->CallVoidMethod(tvZ, setTextID, env->NewStringUTF(viewControllerGlobal->tvZText.c_str()));
        env->CallVoidMethod(tvTotal, setTextID, env->NewStringUTF(viewControllerGlobal->tvTotalText.c_str()));
        env->CallVoidMethod(tvLoop, setTextID, env->NewStringUTF(viewControllerGlobal->tvLoopText.c_str()));
        env->CallVoidMethod(tvFeature, setTextID, env->NewStringUTF(viewControllerGlobal->tvFeatureText.c_str()));
        env->CallVoidMethod(tvBuf, setTextID, env->NewStringUTF(viewControllerGlobal->tvBufText.c_str()));

        jint visibility = viewControllerGlobal->initImageVisible ? VISIBLE : INVISIBLE;
        env->CallVoidMethod(ivInit, setVisibilityID, visibility);
    }
    viewControllerGlobal->viewUpdateMutex.unlock();
}

extern "C"
JNIEXPORT void JNICALL
Java_com_thkoeln_jmoeller_vins_1mobile_1androidport_VinsJNI_onARSwitch(JNIEnv *env, jclass type,
                                                                     jboolean isChecked) {

    if(viewControllerGlobal)
        viewControllerGlobal->switchUI(isChecked);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_thkoeln_jmoeller_vins_1mobile_1androidport_VinsJNI_onLoopSwitch(JNIEnv *env, jclass type,
                                                                       jboolean isChecked) {

    if(viewControllerGlobal)
        viewControllerGlobal->loopButtonPressed(isChecked);
}

cv::Mat
convertToCvRGB(AndroidImage const & androidImg) {
    // TODO needs optimization

    auto rgbFromYUVFunc = [](uint8_t y, uint8_t u, uint8_t v, uint8_t & r,
                             uint8_t & g, uint8_t & b) {
        // https://en.wikipedia.org/wiki/YUV#Y%E2%80%B2UV420sp_(NV21)_to_RGB_conversion_(Android)
        int rTmp = y + (1.370705 * (v - 128));
        int gTmp = y - (0.698001 * (v - 128)) - (0.337633 * (u - 128));
        int bTmp = y + (1.732446 * (u - 128));
        r = (uint8_t) std::min(255, std::max(0, rTmp));
        g = (uint8_t) std::min(255, std::max(0, gTmp));
        b = (uint8_t) std::min(255, std::max(0, bTmp));
    };

    cv::Mat colorImg(androidImg.height, androidImg.width, CV_8UC3);
    for (int y = 0; y < androidImg.height; ++y) {
        for (int x = 0; x < androidImg.width; ++x) {

            uint8_t r, g, b;
            if (androidImg.encoding == AndroidImage::Encoding::YUV420) {
                std::tuple<uint8_t, uint8_t, uint8_t> values = androidImg.getChannelValues(x, y);
                uint8_t yc = std::get<0>(values);
                uint8_t uc = std::get<1>(values);
                uint8_t vc = std::get<2>(values);
                rgbFromYUVFunc(yc, uc, vc, r, g, b);
            }
            else {
                std::tuple<uint8_t, uint8_t, uint8_t> values = androidImg.getChannelValues(x, y);
                uint8_t rc = std::get<0>(values);
                uint8_t gc = std::get<1>(values);
                uint8_t bc = std::get<2>(values);
                r = rc;
                g = gc;
                b = bc;
            }

            cv::Vec3b & bgr = colorImg.at<cv::Vec3b>(y, x);
            bgr[0] = b;
            bgr[1] = g;
            bgr[2] = r;
        }
    }

    return colorImg;
}
